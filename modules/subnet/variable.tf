# subnet vaariables
variable "pub_cidr_block" {
  description = "subnet cidr"
  type = string
  default = "10.0.1.0/24"
}

variable "pub_subnet_name" {
  description = "subnet names"
  type = string
  default = "kibana-subnet"

}

variable "vpc_id" {
  description = "subnet vpc name"
  type = string
}

variable "availability_zone" {
  description = "aavailibility zone for subnet"
  type = string
  default = "us-east-2a"
  
}
